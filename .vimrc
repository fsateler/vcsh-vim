
"au BufNewFile,BufRead *.orc,*.sco,*.csd   so $VIMRUNTIME/syntax/csound.vim
"au BufNewFile,BufRead *.csd               so $VIMRUNTIME/macros/csound_macros.vim

" syntax highlighting
syntax on
set background=dark

if has("autocmd")
  " jump to the last position when reopening a file
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

  " indentation rules
  filetype plugin indent on

endif

set showcmd             " Show (partial) command in status line.
set showmatch           " Show matching brackets.
set ignorecase          " Do case insensitive matching
set smartcase           " Do smart case matching
set incsearch           " Incremental search


"au BufNewFile *.csd     0r $VIMRUNTIME/templates/template.csd

"Enable modelines (vim: blah blah)
set ml

"
" automatically give executable permissions if file begins with #! and 
" contains '/bin/' in the path

"au BufWritePost * if getline(1) =~ "^#!.*/bin/" | silent !chmod +x <afile>

" Do not allow editing in readonly buffers
" http://vim.wikia.com/wiki/Make_buffer_modifiable_state_match_file_readonly_state
function UpdateModifiable()
	if !exists("b:setmodifiable")
		let b:setmodifiable = 0
	endif
	if &readonly
		if &modifiable
			setlocal nomodifiable
			let b:setmodifiable = 1
		endif
	else
		if b:setmodifiable
			setlocal modifiable
		endif
	endif
endfunction
autocmd BufReadPost * call UpdateModifiable()

" Folding in changelog
let g:debchangelog_fold_enable = 1

" Set tab to 4 spaces in python files
au Syntax   python set expandtab sw=4 tabstop=4
au Filetype python set expandtab sw=4 tabstop=4

au Syntax   cmake  set expandtab sw=4 tabstop=4
au Filetype cmake  set expandtab sw=4 tabstop=4

au Syntax   javascript set expandtab sw=4 tabstop=4
au Filetype javascript set expandtab sw=4 tabstop=4

" YCM config
let g:ycm_extra_conf_globlist = ['~/src/systemd/*', '~/src/deb/systemd/systemd/']
